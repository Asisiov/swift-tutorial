//
//  DetailsViewController.swift
//  Restaraunts
//
//  Created by iMac Asisiov  on 4/4/16.
//  Copyright © 2016 iMac Asisiov . All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var restaurantImageView: UIImageView!
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var rateButton: UIButton!
    
    var restaurant = Restaurant(name: "", type: "", location: "", phone: "", image: "", visited: false)
    
    private let _dataCount = 5
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        restaurantImageView?.image = UIImage(named: restaurant.image)
        
        tableView.backgroundColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 0.2)
        tableView.tableFooterView = UIView(frame: CGRectZero)
        tableView.separatorColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 0.8)
        
        //Scaling itself
        tableView.estimatedRowHeight = 36.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        self.title = restaurant.name
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.hidesBarsOnSwipe = false
        navigationController?.setToolbarHidden(false, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        guard segue.identifier == "showMap" else { return }
        let destinationViewController = segue.destinationViewController as! MapViewController
        destinationViewController.restaurant = restaurant
    }
}

extension DetailsViewController
{
    @IBAction func closeAction(segua:UIStoryboardSegue)
    {
        guard let ratingVC = segua.sourceViewController as? RateViewController else { return }
        guard let rating = ratingVC.rating else { return }
        
        rateButton.setImage(UIImage(named: rating), forState: .Normal)
    }
}

extension DetailsViewController: UITableViewDataSource
{
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _dataCount
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! RestaurantCustomTableViewCell
        
        switch indexPath.row
        {
        case 0:
            cell.fieldLabel.text = "Name"
            cell.valueLabel.text = restaurant.name
            break
        case 1:
            cell.fieldLabel.text = "Location"
            cell.valueLabel.text = restaurant.location
            break
        case 2:
            cell.fieldLabel.text = "Type"
            cell.valueLabel.text = restaurant.type
            break
        case 3:
            cell.fieldLabel.text = "Phone Number"
            cell.valueLabel.text = restaurant.phone
            break
        case 4:
            cell.fieldLabel.text = "Been here"
            cell.valueLabel.text = restaurant.isVisited ? "Yes" : "No"
            break
        default:
            cell.fieldLabel.text = ""
            cell.valueLabel.text = ""
            break
        }
        
        return cell
    }
}

extension DetailsViewController: UITableViewDelegate
{
    
}
