//
//  MapViewController.swift
//  Restaraunts
//
//  Created by Aleksandr Sisiov on 4/10/16.
//  Copyright © 2016 iMac Asisiov . All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    var restaurant: Restaurant!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(restaurant.location) { (placemarks, error) in
            
            defer
            {
                if error != nil
                {
                    print(error)
                }
            }
            
            guard error == nil else { return }
            guard let _placemarks = placemarks else { return }
            guard let location = _placemarks[0].location else { return }
            
            self.showAnotationWithLocation(location)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    private func showAnotationWithLocation(location: CLLocation)
    {
        let anotation = MKPointAnnotation()
        anotation.title = restaurant.name
        anotation.subtitle = restaurant.type
        anotation.coordinate = location.coordinate
        
        mapView.showAnnotations([anotation], animated: true)
        mapView.selectAnnotation(anotation, animated: true)
    }
}

extension MapViewController: MKMapViewDelegate
{
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        guard !annotation.isKindOfClass(MKUserLocation) else { return nil }
        
        let identifier = "MyPin"
        
        var annotationView: MKPinAnnotationView? = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier) as? MKPinAnnotationView
        
        if annotationView == nil
        {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.canShowCallout = true
        }
        
        let leftIconView = UIImageView(frame: CGRectMake(0, 0, 53, 53))
        leftIconView.image = UIImage(named: restaurant.image)
        
        annotationView?.leftCalloutAccessoryView = leftIconView
        
        return annotationView
    }
}
