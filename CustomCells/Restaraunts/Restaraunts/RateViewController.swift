//
//  RateViewController.swift
//  Restaraunts
//
//  Created by Aleksandr Sisiov on 4/9/16.
//  Copyright © 2016 iMac Asisiov . All rights reserved.
//

import UIKit

enum Rate: Int
{
    case Rate_Dislike = 1
    case Rate_good
    case Rate_great
}

class RateViewController: UIViewController {

    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var stack: UIStackView!
    
    var rating:String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        transformStack()
        
        setBluerEfect()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animateWithDuration(0.4, delay: 0, options: [], animations: { 
            self.stack.transform = CGAffineTransformIdentity
            }, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    private func setBluerEfect()
    {
        let blurEfect = UIBlurEffect(style: .Dark)
        let blurEfectView = UIVisualEffectView(effect: blurEfect)
        
        var frame = view.bounds
        frame.size.height += 40.0
        
        blurEfectView.frame = frame
        
        backgroundImageView.addSubview(blurEfectView)
    }

    private func transformStack()
    {
        let scale = CGAffineTransformMakeScale(0.0, 0.0)
        let translation = CGAffineTransformMakeTranslation(0.0, 500.0)
        
        stack.transform = CGAffineTransformConcat(scale, translation)
    }
}

extension RateViewController
{
    @IBAction func rateSelectAction(sender: UIButton)
    {
        switch sender.tag {
        case Rate.Rate_Dislike.rawValue: rating = "dislike"
        case Rate.Rate_good.rawValue: rating = "good"
        case Rate.Rate_great.rawValue: rating = "great"
            
        default:
            break
        }
        
        performSegueWithIdentifier("unwindToDeteil", sender: sender)
    }
}
