//
//  RestaurantCustomTableViewCell.swift
//  Restaraunts
//
//  Created by iMac Asisiov  on 4/5/16.
//  Copyright © 2016 iMac Asisiov . All rights reserved.
//

import UIKit

class RestaurantCustomTableViewCell: UITableViewCell {

    @IBOutlet weak var fieldLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!

}
