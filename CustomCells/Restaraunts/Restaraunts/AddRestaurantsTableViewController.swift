//
//  AddRestaurantsTableViewController.swift
//  Restaraunts
//
//  Created by Aleksandr Sisiov on 4/10/16.
//  Copyright © 2016 iMac Asisiov . All rights reserved.
//

import UIKit

class AddRestaurantsTableViewController: UITableViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    private let _cellTagImagePicker = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        guard indexPath.row == _cellTagImagePicker else { return }
        guard UIImagePickerController.isSourceTypeAvailable(.PhotoLibrary) else { return }
        
        showImagePickerController()
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    private func showImagePickerController()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .PhotoLibrary
        imagePicker.delegate = self
        
        presentViewController(imagePicker, animated: true, completion: nil)
    }
}

extension AddRestaurantsTableViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        imageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        imageView.contentMode = .ScaleAspectFill
        imageView.clipsToBounds = true
        
        dismissViewControllerAnimated(true, completion: nil)
    }
}
