//
//  Restaurant.swift
//  Restaraunts
//
//  Created by iMac Asisiov  on 4/4/16.
//  Copyright © 2016 iMac Asisiov . All rights reserved.
//

import Foundation

class Restaurant {
    var name = ""
    var type = ""
    var location = ""
    var image = ""
    var phone = ""
    var isVisited = false
    
    init(name named:String, type typed:String, location locationed:String, phone phoneNumber:String, image imageName:String, visited visit:Bool)
    {
        name = named
        type = typed
        location = locationed
        image = imageName
        phone = phoneNumber
        isVisited = visit
    }
}
