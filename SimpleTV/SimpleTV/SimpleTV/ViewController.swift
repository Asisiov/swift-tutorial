//
//  ViewController.swift
//  SimpleTV
//
//  Created by iMac Asisiov  on 4/1/16.
//  Copyright © 2016 iMac Asisiov . All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let arrayOfLessons = ["Lesson 1","Lesson 2","Lesson 3","Lesson 4","Lesson 5","Lesson 6","Lesson 7" ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}

extension ViewController: UITableViewDataSource
{
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfLessons.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cellID = "Cell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellID, forIndexPath: indexPath)
        
        cell.textLabel?.text = arrayOfLessons[indexPath.row]
        cell.imageView?.image = UIImage(named: "swift")
        
        return cell
    }
}

extension ViewController: UITableViewDelegate
{
}

